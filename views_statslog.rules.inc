<?php
/**
 * Rules integration for views statslog.
 */

/**
 * Implements hook_rules_action_info().
 */
function views_statslog_rules_action_info() {
  $actions = array();
  $actions['views_statslog_rules_collect_action'] = array(
    'label' => t('Collect statslog data'),
    'group' => t('Views statslog'),
  );
  return $actions;
}

/**
 * Execution callback.
 */
function views_statslog_rules_collect_action($settings, $state, $action, $hook) {
  foreach (views_statslog_events($settings['enabled_views']) as $event) {
    watchdog('views_statslog', "!data", array('!data' => json_encode($event)));
  }
}

/**
 * Configuration form.
 */
function views_statslog_rules_collect_action_form_alter(&$form, &$form_state, $options, RulesAction $action, $hook) {
  if (!isset($action->settings['enabled_views'])) {
    $action->settings['enabled_views'] = array();
  }
  $form['enabled_views'] = _views_statslog_widget($action->settings['enabled_views']);
  $form['#validate'] = array('_views_statslog_rules_action_form_validate');
}
